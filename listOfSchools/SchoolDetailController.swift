//
//  SchoolDetailController.swift
//  listOfSchools
//
//  Created by Ashish Stephen on 8/29/23.
//

import Foundation
import UIKit
import CoreLocation

class SchoolDetailController : UIViewController {
    var schoolDataModel: SchoolData?
    var schoolSatData: [SchoolSatData]?
    var schoolDBN: String = ""
    let geoCoder = CLGeocoder()
    @IBOutlet weak var overview: UILabel!
    
    @IBOutlet weak var address: UIButton!
    @IBOutlet weak var contactInfo: UILabel!
    
    @IBOutlet weak var satCriticalReadingAvgScore: UILabel!
    @IBOutlet weak var satMathAvgScore: UILabel!
    @IBOutlet weak var satWritingAvgScore: UILabel!
    
    @IBOutlet weak var contactNumber: UIButton!
    @IBOutlet weak var email: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = schoolDataModel?.school_name
        self.navigationItem.leftBarButtonItem?.title = ""
        getSatDataForSchools()
        setupSchoolDetail()
    }
    
    
    @IBAction func openEmail(_ sender: Any) {
        let urlString = self.email.titleLabel?.text
        if let url = URL(string: "mailto:\(urlString ?? "")?subject=&body="),
           UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func openMaps(_ sender: Any) {
        let schoolLocation = (self.address.titleLabel?.text ?? "") + "," + (self.schoolDataModel?.city ?? "") + ","
        let cityZip = (self.schoolDataModel?.state_code ?? "") + "," + (self.schoolDataModel?.zip ?? "")
        let location = schoolLocation + cityZip
        geoCoder.geocodeAddressString(location) { (placemarks, error) in
            guard let placemarks = placemarks?.first else { return }
            let location = placemarks.location?.coordinate ?? CLLocationCoordinate2D()
            guard let url = URL(string:"http://maps.apple.com/?daddr=\(location.latitude),\(location.longitude)") else { return }
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func makeACall(_ sender: Any) {
        guard let phoneNumber = self.contactNumber.titleLabel?.text else {return}
        let numberUrl = URL(string: "tel://\(phoneNumber)") ?? URL(fileURLWithPath: "")
        if UIApplication.shared.canOpenURL(numberUrl) {
            UIApplication.shared.open(numberUrl)
        }
    }
    
    func setupSchoolDetail() {
        guard let schoolData = schoolDataModel else {return}
        self.schoolDBN = schoolData.dbn ?? ""
        self.email.titleLabel?.text = ""
        self.contactNumber.titleLabel?.text = ""
        self.satMathAvgScore.text = "N/A"
        self.satWritingAvgScore.text = "N/A"
        self.satCriticalReadingAvgScore.text = "N/A"
        self.address.titleLabel?.text = schoolData.primary_address_line_1! + ", " + schoolData.city! + ", " + schoolData.zip!
        self.overview.text = "\"\(schoolData.overview_paragraph!)\""
        self.contactInfo.text = "Contact Info"
        self.email.setTitle((schoolData.school_email ?? ""), for: .normal)
        self.contactNumber.setTitle((schoolData.phone_number ?? ""), for: .normal)
        self.address.setTitle((schoolData.primary_address_line_1), for: .normal)
    }
    
    func updateUI() {
        let schoolSatScores = self.schoolSatData?.filter({ $0.dbn == self.schoolDBN }).first
        if schoolSatScores != nil {
            self.satMathAvgScore.text = schoolSatScores?.satMathAvgScore
            self.satWritingAvgScore.text = schoolSatScores?.satWritingAvgScore
            self.satCriticalReadingAvgScore.text = schoolSatScores?.satCriticalReadingAvgScore
        }
    }
    
}

extension SchoolDetailController {
    func getSatDataForSchools() {
        
        let urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

        // Create a URL from the string
        guard let url = URL(string: urlString) else {
            print("Invalid URL")
            exit(1)
        }

        // Create a URLSession
        let session = URLSession.shared

        // Create a data task to fetch the data
        let task = session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("Error: \(error)")
                return
            }
            
            guard let data = data else {
                print("No data received")
                return
            }
            
            // Parse JSON data into an array of Post objects
            do {
                let decoder = JSONDecoder()
                self.schoolSatData = try decoder.decode([SchoolSatData].self, from: data)
                
                // Print the parsed posts
                print(self.schoolSatData)
                
                DispatchQueue.main.async {
                    self.updateUI()
                }
                
            } catch {
                print("Error decoding JSON: \(error)")
            }
        }

        // Start the data task
        task.resume()
    }
}
