//
//  SchoolModel.swift
//  listOfSchools
//
//  Created by Ashish Stephen on 8/29/23.
//

import Foundation

class SchoolData: Codable {
    let zip : String?
    let website : String?
    let total_students : String?
    let subway : String?
    let state_code : String?
    let start_time : String?
    let shared_space : String?
    let seats9swd1 : String?
    let seats9ge1 : String?
    let seats101 : String?
    let school_name : String?
    let school_email : String?
    let school_accessibility_description : String?
    let psal_sports_girls : String?
    let psal_sports_coed : String?
    let psal_sports_boys : String?
    let program1 : String?
    let primary_address_line_1 : String?
    let phone_number : String?
    let pct_stu_safe : String?
    let pct_stu_enough_variety : String?
    let overview_paragraph : String?
    let offer_rate1 : String?
    let nta : String?
    let neighborhood : String?
    let method1 : String?
    let longitude : String?
    let location : String?
    let latitude : String?
    let language_classes : String?
    let interest1 : String?
    let graduation_rate : String?
    let grades2018 : String?
    let grade9swdfilledflag1 : String?
    let grade9swdapplicantsperseat1 : String?
    let grade9swdapplicants1 : String?
    let grade9gefilledflag1 : String?
    let grade9geapplicantsperseat1 : String?
    let grade9geapplicants1 : String?
    let girls : String?
    let finalgrades : String?
    let fax_number : String?
    let extracurricular_activities : String?
    let end_time : String?
    let ell_programs : String?
    let eligibility1 : String?
    let diplomaendorsements : String?
    let dbn : String?
    let council_district : String?
    let community_board : String?
    let college_career_rate : String?
    let code1 : String?
    let city : String?
    let census_tract : String?
    let bus : String?
    let building_code : String?
    let borough : String?
    let boro : String?
    let bin : String?
    let bbl : String?
    let attendance_rate : String?
    let admissionspriority21 : String?
    let admissionspriority11 : String?
    let addtl_info1 : String?
    let academicopportunities5 : String?
    let academicopportunities4 : String?
    let academicopportunities3 : String?
    let academicopportunities2 : String?
    let academicopportunities1 : String?
}

struct SchoolSatData: Codable {
    let dbn, schoolName, numOfSatTestTakers, satCriticalReadingAvgScore: String
    let satMathAvgScore, satWritingAvgScore: String

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}
