//
//  ViewController.swift
//  listOfSchools
//
//  Created by Ashish Stephen on 8/29/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var schoolsTable: UITableView!
    var schoolList: [SchoolData] = []
    var isSearchOn = false
    var filteredSchoolList: [SchoolData]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "List of NYC schools"
        getSchoolList()
    }
}


// MARK: Table delegates

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrayCount = isSearchOn ? self.filteredSchoolList?.count : self.schoolList.count
        return arrayCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = schoolsTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SchoolCell
        
        let array = isSearchOn ? self.filteredSchoolList : self.schoolList
        guard let schoolData = array?[indexPath.row] else {
            return cell
        }
        
        cell.schoolNameLabel.text = schoolData.school_name
        cell.cityName.text = schoolData.city
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SchoolDetailController {
            vc.schoolDataModel = sender as? SchoolData
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let array = isSearchOn ? self.filteredSchoolList : self.schoolList
        performSegue(withIdentifier: "showSchoolDetail", sender: array?[indexPath.row])
    }
}

extension ViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty == false {
            isSearchOn = true
            self.filteredSchoolList = self.schoolList.filter{ $0.school_name?.contains(searchText) ?? false || $0.city!.contains(searchText) }
        } else {
            isSearchOn = false
        }
        self.schoolsTable.reloadData()
    }
}

// MARK: JSON parse

extension ViewController {
    func getSchoolList() {
        var spinner = UIActivityIndicatorView(style: .large)
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"

        // Create a URL from the string
        guard let url = URL(string: urlString) else {
            print("Invalid URL")
            exit(1)
        }

        // Create a URLSession
        let session = URLSession.shared

        // Create a data task to fetch the data
        let task = session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("Error: \(error)")
                return
            }
            
            guard let data = data else {
                print("No data received")
                return
            }
            
            // Parse JSON data into an array of Post objects
            do {
                let decoder = JSONDecoder()
                self.schoolList = try decoder.decode([SchoolData].self, from: data)
                
                // Print the parsed posts
                print(self.schoolList)
                
                DispatchQueue.main.async {
                    self.schoolsTable.reloadData()
                }
                
            } catch {
                print("Error decoding JSON: \(error)")
            }
        }

        // Start the data task
        task.resume()
    }
}

