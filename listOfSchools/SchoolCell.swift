//
//  SchoolCell.swift
//  listOfSchools
//
//  Created by Ashish Stephen on 8/29/23.
//

import Foundation
import UIKit

class SchoolCell: UITableViewCell {
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var cityName: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    
}
